package com.j4zz1x.servernotificationsystem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class MainActivity extends AppCompatActivity {

    private NotificationManagerCompat managerCompat;

    private Handler mHandlerSmoke = new Handler();

    ImageView about;

    int temp, hum, smoke, smokeSaved, water;
    String tempValue, humValue, smokeValue, waterValue;
    TextView tempViewer, humViewer, smokeViewer, waterViewer;
    TextView tempTextUn, humTextUn, smokeTextUn, waterTextUn;
    CardView tempCard, humCard, smokeCard, waterCard;
    DatabaseReference reff;

    int status = 0;
    int i = 0;
    int n = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        managerCompat = NotificationManagerCompat.from(this);

        timer();
        if (i == 180000){
            smokeSaved = smoke;
        }

        //Testing Connection
        //Toast.makeText(MainActivity.this,"firebase connected!",Toast.LENGTH_LONG).show();
        about = (ImageView) findViewById(R.id.imageView);
        tempCard = findViewById(R.id.crdTemp);
        humCard = findViewById(R.id.crdHum);
        smokeCard = findViewById(R.id.crdSmoke);
        waterCard = findViewById(R.id.crdWater);
        tempViewer = (TextView)findViewById(R.id.tempView);
        tempTextUn = (TextView) findViewById(R.id.tempText);
        humViewer = (TextView)findViewById(R.id.humView);
        humTextUn = (TextView) findViewById(R.id.humText);
        smokeViewer = (TextView)findViewById(R.id.smokeView);
        smokeTextUn = (TextView) findViewById(R.id.smokeText);
        waterViewer = (TextView)findViewById(R.id.waterView);
        waterTextUn = (TextView) findViewById(R.id.waterText);

        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAbout();
            }
        });

        reff = FirebaseDatabase.getInstance().getReference().child("Sensor");
        reff.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                tempValue = dataSnapshot.child("tempVal").getValue().toString();
                humValue = dataSnapshot.child("humVal").getValue().toString();
                smokeValue = dataSnapshot.child("smokeVal").getValue().toString();
                waterValue = dataSnapshot.child("waterVal").getValue().toString();
                tempViewer.setText(tempValue +"°C");
                humViewer.setText(humValue + "%");
                temp = Integer.parseInt(tempValue);
                hum = Integer.parseInt(humValue);
                smoke = Integer.parseInt(smokeValue);
                water = Integer.parseInt(waterValue);
                Notif();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }

        });

    }

    private void Notif(){
        if (temp > 27 || temp < 18){
            status = 1;
            tempTextUn.setTextColor(Color.parseColor("#ffffff"));
            tempCard.setCardBackgroundColor(Color.parseColor("#d48794"));
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, AppNotification.CHANNEL_1_ID)
                    .setContentTitle("Please Check The Server Room!")
                    .setSmallIcon(R.drawable.ic_warning_white_24dp)
                    .setContentText("Temperature : "+ temp +"°C")
                    .setAutoCancel(false)
                    .setVibrate(new long[] { 1000, 3000})
                    .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                    .setCategory(NotificationCompat.CATEGORY_ALARM);

            managerCompat.notify(1, builder.build());

        }
        if (temp <= 27 && temp >= 18){
            status = 0;
            tempTextUn.setTextColor(Color.parseColor("#a5a5a5"));
            tempCard.setCardBackgroundColor(Color.parseColor("#ffffff"));
        }

        if (hum > 60 || hum < 40){
            status = 1;
            humTextUn.setTextColor(Color.parseColor("#ffffff"));
            humCard.setCardBackgroundColor(Color.parseColor("#d48794"));
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, AppNotification.CHANNEL_2_ID)
                    .setContentTitle("Please Check The Server Room!")
                    .setSmallIcon(R.drawable.ic_warning_white_24dp)
                    .setContentText("humidity : "+ hum +"%")
                    .setAutoCancel(false)
                    .setVibrate(new long[] { 1000, 3000})
                    .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                    .setCategory(NotificationCompat.CATEGORY_ALARM);

            managerCompat.notify(2, builder.build());
        }
        if (hum <= 60 && hum >= 40){
            status = 0;
            humTextUn.setTextColor(Color.parseColor("#a5a5a5"));
            humCard.setCardBackgroundColor(Color.parseColor("#ffffff"));
        }

        if((smokeSaved < smoke) && (i == 180000)){
            status = 1;
            smokeTextUn.setTextColor(Color.parseColor("#ffffff"));
            smokeCard.setCardBackgroundColor(Color.parseColor("#d48794"));
            smokeViewer.setText("Smoke Detected!");
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, AppNotification.CHANNEL_3_ID)
                    .setContentTitle("Please Check The Server Room!")
                    .setSmallIcon(R.drawable.ic_warning_white_24dp)
                    .setContentText("Smoke Detected!")
                    .setAutoCancel(false)
                    .setVibrate(new long[] { 1000, 3000})
                    .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                    .setCategory(NotificationCompat.CATEGORY_ALARM);

            managerCompat.notify(3, builder.build());
        }

        if (smoke < smokeSaved){
            status = 0;
            smokeTextUn.setTextColor(Color.parseColor("#a5a5a5"));
            smokeViewer.setText("Not Detected");
            smokeCard.setCardBackgroundColor(Color.parseColor("#ffffff"));
        }
        if (water > 100){
            status = 1;
            waterTextUn.setTextColor(Color.parseColor("#ffffff"));
            waterCard.setCardBackgroundColor(Color.parseColor("#d48794"));
            waterViewer.setText("Water Detected!");
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, AppNotification.CHANNEL_4_ID)
                    .setContentTitle("Please Check The Server Room!")
                    .setSmallIcon(R.drawable.ic_warning_white_24dp)
                    .setContentText("Water Detected!")
                    .setAutoCancel(false)
                    .setVibrate(new long[] { 1000, 3000})
                    .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                    .setCategory(NotificationCompat.CATEGORY_ALARM);

            managerCompat.notify(4, builder.build());
        }
        if (water < 100){
            status = 0;
            waterTextUn.setTextColor(Color.parseColor("#a5a5a5"));
            waterViewer.setText("Not Detected");
            waterCard.setCardBackgroundColor(Color.parseColor("#ffffff"));
        }
    }

    public void openAbout(){
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    private void timer(){
        while(i != 180000){
            i++;
        }
    }
}