package com.j4zz1x.servernotificationsystem;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;

public class AppNotification extends Application {

    public static final String CHANNEL_1_ID = "channel1";
    public static final String CHANNEL_2_ID = "channel2";
    public static final String CHANNEL_3_ID = "channel3";
    public static final String CHANNEL_4_ID = "channel4";

    @Override
    public void onCreate() {
        super.onCreate();
        createNofiticationChannels();
    }
    private void createNofiticationChannels(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel channel1 = new NotificationChannel(
                    CHANNEL_1_ID,"Temperature", NotificationManager.IMPORTANCE_HIGH);
            channel1.setDescription("Test Channel 1");
            NotificationChannel channel2 = new NotificationChannel(
                    CHANNEL_2_ID,"Humidity", NotificationManager.IMPORTANCE_HIGH);
            channel1.setDescription("Test Channel 2");
            NotificationChannel channel3 = new NotificationChannel(
                    CHANNEL_3_ID,"Smoke", NotificationManager.IMPORTANCE_HIGH);
            channel1.setDescription("Test Channel 3");
            NotificationChannel channel4 = new NotificationChannel(
                    CHANNEL_4_ID,"Smoke", NotificationManager.IMPORTANCE_HIGH);
            channel1.setDescription("Test Channel 4");

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel1);
            manager.createNotificationChannel(channel2);
            manager.createNotificationChannel(channel3);
            manager.createNotificationChannel(channel4);
        }
    }
}