#include <Firebase.h>
#include <FirebaseArduino.h>
#include <FirebaseCloudMessaging.h>
#include <FirebaseError.h>
#include <FirebaseHttpClient.h>
#include <FirebaseObject.h>
#include <SoftwareSerial.h>
#include <ESP8266WiFi.h>
#include <ArduinoJson.h>
#include "DHT.h"

//Sensor
#define DHTPIN D4
#define DHTTYPE DHT11
DHT dht2(DHTPIN, DHTTYPE);

#define water_Power D7 //PIN USED WaterLevel
#define water_Pin A0 //PIN USED WaterLevel
int water_Val = 0;

//Firebase
#define FIREBASE_HOST "serversensor-j4zz1x.firebaseio.com"
#define FIREBASE_AUTH "BOCy6KzlUzExYnzrn3F9Ou3DKvmF3aB5Qi89oPt9"

//Wifi
#define WIFI_SSID "HomeC21"
#define WIFI_PASSWORD "randomc21"

void setup()
{ 
  // Set D7 as an OUTPUT
  pinMode(water_Power, OUTPUT);
  
  // Set to LOW so no power flows through the sensor
  digitalWrite(water_Power, LOW);
  
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  while(WiFi.status()!= WL_CONNECTED)
  {
    Serial.print(".");
    delay(500);
  }
  Serial.println("Connected!");
  Serial.print("IP Address: ");
  Serial.print(WiFi.localIP());
  Serial.print("\n");

  //Firebase
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
  
}

void loop()
{
  //Temp & Hum
  int temp = dht2.readTemperature();
  int hum = dht2.readHumidity();

  //Water
  int water = readSensor();
  
  if (isnan(temp) || isnan(hum)) {  // Check if any reads failed and exit early
    Serial.println(F("Failed to read from DHT sensor!"));
    delay(5000);
    return;
  }
  if (isnan(water)) {  // Check if any reads failed and exit early
    Serial.println(F("Failed to read from Water Level sensor!"));
    delay(5000);
    return;
  }
  
  Serial.print("\nTemperature = ");
  Serial.print(temp);
  Serial.print("C ");
  Serial.print("\n");
  Serial.print("Humidity = ");
  Serial.print(hum);
  Serial.print("%\n");
  Serial.print("\nWater Level = ");
  Serial.print(water);
  Serial.print("\n");
  
  //Firebase
  Firebase.setString("Sensor/humVal", String(hum));
  delay(300);
  Firebase.setString("Sensor/tempVal", String(temp));
  delay(300);
  Firebase.setString("Sensor/waterVal", String(water));
  delay(300);
  
}

int readSensor() {
  digitalWrite(water_Power, HIGH);  // Turn the sensor ON
  delay(10);              // wait 10 milliseconds
  water_Val = analogRead(water_Pin);    // Read the analog value form sensor
  digitalWrite(water_Power, LOW);   // Turn the sensor OFF
  return water_Val;             // send current reading
}
