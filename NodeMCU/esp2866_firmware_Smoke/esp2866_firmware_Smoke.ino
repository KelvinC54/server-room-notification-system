#include <Firebase.h>
#include <FirebaseArduino.h>
#include <FirebaseCloudMessaging.h>
#include <FirebaseError.h>
#include <FirebaseHttpClient.h>
#include <FirebaseObject.h>
#include <SoftwareSerial.h>
#include <ESP8266WiFi.h>
#include <ArduinoJson.h>

//Sensor
#define smoke_Pin A0 //PIN USED MQ2

//Firebase
#define FIREBASE_HOST "serversensor-j4zz1x.firebaseio.com"
#define FIREBASE_AUTH "BOCy6KzlUzExYnzrn3F9Ou3DKvmF3aB5Qi89oPt9"

//Wifi
#define WIFI_SSID "HomeC21"
#define WIFI_PASSWORD "randomc21"

void setup()
{ 
  Serial.begin(9600);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  while(WiFi.status()!= WL_CONNECTED)
  {
    Serial.print(".");
    delay(500);
  }
  Serial.println("Connected!");
  Serial.print("IP Address: ");
  Serial.print(WiFi.localIP());
  Serial.print("\n");

  //Firebase
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
  
}

void loop()
{
  //Smoke
  int smoke = analogRead(smoke_Pin);
  
  if (isnan(smoke)) {  // Check if any reads failed and exit early (to try again).
    Serial.println(F("Failed to read from DHT sensor!"));
    delay(5000);
    return;
  }
  Serial.print("\n\nSmoke Level = ");
  Serial.print(smoke);

  
  //Firebase
  Firebase.setString("Sensor/smokeVal", String(smoke));
  delay(1000);
  
}
